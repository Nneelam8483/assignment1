To run the project, you should have
1. Visual Studion 2019.
2. .NET Core 3.7.

Follow the steps:
1. Open the project in  Visual Studio 2019
2. Click the run button.

Reason to choose MIT license:
The MIT license is chosen by me because it allows us to share the code under a license without forcing others to expose their proprietary code. It's business friendly and open source friendly as well.